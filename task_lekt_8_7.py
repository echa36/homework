# Описать функцию is_power_n( k , n ) логического типа, возвращающую
# True, если целый параметр k (> 0) является степенью числа n (> 1), и False
# в противном случае. Дано число n (> 1) и набор из 10 целых положитель-
# ных чисел. С помощью функции is_power_n найти количество степеней чис-
# ла N в данном наборе.


lst_in = [2, 4, 16, 3, 256, 12, 8, 5, 17179869184, 14]
n = 2


def is_power_n(k, m):
    while k != m:
        if k % m == 0:
            k = k / m
        elif k % m != 0:
            k = m
            return False
    return True


lst_0ut = [i for i in lst_in if is_power_n(i, n)]
print(len(lst_0ut))
