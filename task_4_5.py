# Составить список чисел Фибоначчи содержащий 15 элементов.


num_1 = int(input('Введите число\n'))
lst_1 = [num_1]  # создаем новый список
for elem in range(15 - 1):
    if elem > 0:
        lst_1.append(lst_1[elem] + lst_1[elem - 1])  # добавляем элементы в новый список
    else:
        lst_1.append(lst_1[elem])
print(lst_1)

num_1 = int(input('Введите число\n'))
lst_1 = [num_1]
elem = 0
while elem < (15 - 1):
    if elem > 0:
        lst_1.append(lst_1[elem] + lst_1[elem - 1])
    else:
        lst_1.append(lst_1[elem])
    elem += 1
print(lst_1)

