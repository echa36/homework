# Дан словарь: {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
# Добавить каждому ключу число равное длине этого ключа (пример {‘key’: ‘value’} -> {‘key3’:
# ‘value’}). Чтобы получить список ключей - использовать метод .keys()

dict_1 = {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
lst_1 = list(dict_1.keys())  # преобразуем в список ключи словаря
for elem in lst_1:
    new_key = elem + str(len(elem))
    dict_1[new_key] = dict_1[elem]  # присваиваем значение новому ключу
    del dict_1[elem]  # удаляем старый ключ
print(dict_1)

dict_1 = {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
lst_1 = list(dict_1.keys())
elem = 0
while elem < len(lst_1):
    new_key = lst_1[elem] + str(len(lst_1[elem]))
    dict_1[new_key] = dict_1[(lst_1[elem])]
    del dict_1[(lst_1[elem])]
    elem += 1
print(dict_1)

