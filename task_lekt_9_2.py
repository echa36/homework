# Создать lambda функцию, которая
# принимает на вход список имен и
# выводит их в формате “Hello, {name}” в
# другой список

lst_in = ['Mike', 'Nik', 'Jon']
lst_out = [(lambda x: f'Hello, {x}')(x=i) for i in lst_in]
print(lst_out)

