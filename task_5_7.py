# Дана целочисленная квадратная матрица. Найти в каждой строке наи-
# больший элемент и поменять его местами с элементом главной диагонали.

matrix_1 = [[6, 2, 3, 4],
            [4, 5, 6, 9],
            [7, 30, 9, 15],
            [16, 17, 18, 19]]

lst = []  # создаем пустой список
total = 0  # заводим счетчик
for i in matrix_1:
    var1 = i[0]  # присваиваем переменной значение 1-го элемента строки матрицы
    for j in i[1:]:  # сравниваем следующий элемент с предидущим
        if j > var1:  # если он больше то меняем значение переменной
            var1 = j
    matrix_1[total][total] = var1  # присваиваем значению элемента главной диагонали матрицы знач-е пер-ой
    total += 1  # увеличиваем значение счетчика для использования на следующей строке
#  производим печать измененной матрицы
for i in matrix_1:
    for j in i:
        print(j, end=' ')
    print()

