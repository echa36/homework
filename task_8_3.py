# Дано число 28340928374 посчитать сумму цифр этого числа (генератор списков)


from functools import reduce  # импортируем модуль

n = 28340928374
"""
here we convert the number to a list, then count the sum
"""
result = reduce(lambda a, x: a + x, list(map(lambda x: int(x), str(n))), 0)
print(result)

