# Создать список поездов. Структура словаря: номер поезда,
# пункт и время прибытия, пункт и время отбытия. Вывести все сведения о поездах,
# время пребывания в пути которых превышает 7 часов 20 минут.[02-7.3-ML02]
# Примечание: данное задание подразумевает самостоятельное изучение принципов работы со
# временем в Python(библиотека datetime)


import datetime  # импорт модуля дататайм

# присвоение переменным времени прибытия и отправления в формате дататайм

train1_depart = datetime.datetime(2021, 1, 25, 7, 30)
train1_arrive = datetime.datetime(2021, 1, 25, 15, 30)
train2_depart = datetime.datetime(2021, 1, 25, 8, 30)
train2_arrive = train2_depart + datetime.timedelta(hours=7, minutes=30)
train3_depart = datetime.datetime(2021, 1, 25, 9, 30)
train3_arrive = train3_depart + datetime.timedelta(hours=4)

# создание словаря с переменными дататайм

schedule = {1: ['Пункт отбытия: Минск', 'Время отбытия:', train1_depart,
                'Пункт прибытия: Москва', 'Время прибытия:', train1_arrive],
            2: ['Пункт отбытия: Минск', 'Время отбытия:', train2_depart,
                'Пункт прибытия: Киев', 'Время прибытия:', train2_arrive],
            3: ['Пункт отбытия: Минск', 'Время отбытия:', train3_depart,
                'Пункт прибытия: Гомель', 'Время прибытия:', train3_arrive]}

# вывод данных о поездах время нахождения в пути которых больше 7 часов 20 минут
for value in schedule.values():
    delta = value[5] - value[2]
    if delta.seconds / 3600 > 7.3:
        print(value[0])
        print(value[1])
        print(value[2].strftime("%Y-%m-%d-%H.%M"))
        print(value[3])
        print(value[4])
        print(value[5].strftime("%Y-%m-%d-%H.%M"))

