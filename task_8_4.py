# Найти самое часто встречающееся слово в строке(String will
# never be empty and you do not need to account for different
# data types and string that not stupid data type in python.
# That string.)

str_in = 'String will never be empty and you do not need to' \
         ' account for different data types and string that' \
         ' not stupid data type in python. That string.'
# converting a string to a list
str_conversion = (''.join(str_in.lower().split('.'))).split()
# next we get a dictionary with a key equal to the maximum number
# of repetitions and a word as the value of this key that is repeated most often
dict_1 = {str_conversion.count(elem): str_conversion[i] for i, elem in enumerate(str_conversion)}
print(dict_1[max(dict_1)])

