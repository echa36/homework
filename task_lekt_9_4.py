# Дан список имен, отфильтровать все имена,
# где есть буква k


lst_in = ['Mike', 'Nik', 'Jon']

result = filter(lambda x: 'k' in x, lst_in)
print(list(result))
