# Написать функцию по решению квадратных
# уравнений.

# ax²+bx+c=0, x=(-b±√D)/2a, D=b²-4ac

str_in = '1x²+3x-4=0'


def quad_aq(st):
    sign = {'+': 1, '-': -1}
    if str_in[0].isdigit():
        a = int(str_in[0])
        b = int(str_in[4])
        c = int(str_in[7])
        s_b = str(str_in[3])
        s_c = str(str_in[6])
    else:
        a = -int(str_in[1])
        b = int(str_in[5])
        c = int(str_in[8])
        s_b = str(str_in[4])
        s_c = str(str_in[7])
    disk = b ** 2 - 4 * a * sign[s_c] * c
    print('1x²+3x-4=0')
    if disk < 0:
        print('Нет решений')
    elif disk > 0:
        x1 = (-b * sign[s_b] + disk ** 0.5) / 2 * a
        x2 = (-b * sign[s_b] - disk ** 0.5) / 2 * a
        print('х1 =', x1)
        print('х2 =', x2)
    elif disk == 0:
        x1 = x2 = (-b * sign[s_b]) / 2 * a
        print('х1 = x2 =', x1)


quad_aq(str_in)
