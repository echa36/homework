# Дан словарь, создать новый словарь, поменяв местам
# ключ и значение

dict_in = {'1': '2001', '2': '2002', '3': '2003'}
dict_out = {value: key for key, value in dict_in.items()}
print(dict_out)
