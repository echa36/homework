# Два натуральных числа называют дружественными, если каждое из них
# равно сумме всех делителей другого, кроме самого этого числа. Найти все
# пары дружественных чисел, лежащих в диапазоне от 200 до 300.

num_1 = 200
num_2 = 300
lst1 = []  # создаем пустые списки
lst2 = []
for elem in range(num_1, num_2):  # запускаем цикл проверки всех чисел от 200 до 300
    for dev_1 in range(1, elem):  # запускаем цикл на поиск всех делителей данного элемента
        if elem % dev_1 == 0:
            lst1.append(dev_1)  # добавляем все делители в список 1
    for j in range(1, sum(lst1)):  # запускаем цикл поиск всех делителей от 1 до суммы всех элементов lst1
        if sum(lst1) % j == 0:
            lst2.append(j)  # добавляем все делители в список 2
    if sum(lst2) == elem:  # если сумма списка 2 = проверяемому элементу
        print(elem)  # вывести элемент
    lst1.clear()
    lst2.clear()

