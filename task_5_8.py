# В заданной строке расположить в обратном порядке все слова. Разделителями
# слов считаются пробелы.


my_text_1 = 'Hello my world and'
a = my_text_1.split(' ')
a.reverse()
print(" ".join(a))

# Второй вариант

my_text_2 = 'Hello my world fnd ghj'
n_text = my_text_2.split(' ')
i = 0
for elem in range(len(n_text) // 2):  # количество циклов = целочисленному делению на 2
    # замена элементов 1-го с последним, 2-го с предпоследним и т д
    n_text[elem], n_text[len(n_text) - 1 - i] = n_text[len(n_text) - 1 - i], n_text[elem]
    i += 1
print(" ".join(n_text))

