# Дан список целых чисел. Подсчитать сколько четных чисел в списке

lst_1 = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
even_number = 0
for elem in lst_1:
    if elem % 2 == 0:
        even_number += 1
print(even_number)

lst_1 = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
elem = 0
even_number = 0
while elem < len(lst_1):
    if lst_1[elem] % 2 == 0:
        even_number += 1
    elem += 1
print(even_number)

