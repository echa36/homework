a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = []
for element in a:
    b.append(element*(-2))
print(b)

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = []
i = 0
while i < len(a):
    b.append(a[i]*(-2))
    i += 1
print(b)
