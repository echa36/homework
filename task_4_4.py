# Дан список. Создать новый список, сдвинутый на 1 элемент влево

lst_1 = [1, 2, 3, 4, 5]
lst_2 = []
count = 0
for i in lst_1:
    if count > 0:
        lst_2.append(i)
    else:
        pass
    count += 1
lst_2.append(lst_1[0])
print(lst_2)

lst_1 = [1, 2, 3, 4, 5]
lst_2 = []
i = 0
while i < len(lst_1):
    if i > 0:
        lst_2.append(lst_1[i])
    else:
        pass
    i += 1
lst_2.append(lst_1[0])
print(lst_2)

