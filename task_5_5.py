# В массиве целых чисел с количеством элементов 19 определить максимальное
# число и заменить им все четные по значению элементы.

arr = [1, 2, -3, 4, 5, -6, 5, 4, 3, -1, 2, 9, 4, 5, 6, 5, 4, 3, 2]
maximum = (max(arr))  # определяем максимальное число
i = 0
for elem in arr:
    if elem % 2 == 0:
        arr[i] = maximum  # меняем четный элемент на максимальный
    i += 1
print(arr)

