# Дан список чисел. Посчитать сколько раз
# встречается каждое число. Использовать
# функцию.

lst_in = [1, 1, 2, 3, 5, 7, 5, 3, 2, 5, 6, 8, 9, 2, 2, 5, 6]


# the first solution
def count_num(l_1, i, c=0):
    for j in l_1:
        if j == i:
            c += 1
    return c


dict_out = {i: count_num(lst_in, i) for i in lst_in}
print(dict_out)

# the second solution
dict_out = {i: lst_in.count(i) for i in lst_in}
print(dict_out)

# the third solution
dict_2 = {}
for el in lst_in:
    if dict_2.get(el, 0) == 0:
        dict_2[el] = count_num(lst_in, el)
print(dict_2)

# the fourth solution
dict_3 = {}
for el in lst_in:
    if dict_3.get(el, 0) == 0:
        dict_3[el] = 1
        count_1 = 0
    elif dict_3.get(el, 0) != 0:
        count_1 = dict_3[el]
        dict_3[el] = count_1 + 1
print(dict_3)
