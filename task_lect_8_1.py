# Дан список слов. Сгенерировать новый список с
# перевернутыми словами

str_in = 'merge incoming changes to the current branch'
lst_in = str_in.split()
lst_out = [i[::-1] for i in lst_in]
print(lst_out)

lst_out = [''.join(reversed(i)) for i in lst_in]
print(lst_out)
