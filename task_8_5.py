# найти самое длинное слово в строке

str_in = 'String will never be empty and you do not need to' \
         ' account for different data types and string that' \
         ' not stupid data type in python. That string.'
# converting a string to a list
str_conversion = (''.join(str_in.lower().split('.'))).split()
# next we get a dictionary with keys equal to the length of
# the words and the word as the value of this key
dict_1 = {len(str_conversion[i]): str_conversion[i] for i, elem in enumerate(str_conversion)}
print(dict_1[max(dict_1)])

